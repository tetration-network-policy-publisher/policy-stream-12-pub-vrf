# Policy-Stream-12-pub-vrf
## Version
Tetration 3.1.1.53, Patch 3.1.1.55
## Updates
This version of Tetration changes the Kafka broker port number to 443 and the naming convention of the Kafka topic.